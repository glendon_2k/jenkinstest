"""
统计dataset图片和标签数量
"""
import os
path_img = 'dataset\images' # images
path_lab = 'dataset\labels' # labels

files_img = os.listdir(path_img) # 读入文件夹
files_lab = os.listdir(path_lab)   

num_img = len(files_img) # 统计文件夹中的文件个数
num_lab = len(files_lab)

# print(num_img, num_lab)

if num_img == num_lab:
    print("numbers of images and labels are matching")
    print("the number of images is", num_img)
else:
    print("numbers of images and labels are not matching")

