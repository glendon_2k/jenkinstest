Jenkinsfile (Declarative Pipeline)
pipeline {
    agent any
    stages {
        stage('Test') {
            steps {
                sh 'python data_check.py'
                echo 'hello'
            }
        }
        stage('Run') {
            steps {
                sh 'python run.py'
            }
        }
    }

    post {
        always {
            echo 'finish'
        }
    }
}